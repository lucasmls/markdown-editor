# markdown-editor

> A markdown editor built with ReactJS. To see the application running, go to: http://react-js-markdown-editor.surge.sh/

## Course
> So far, the course i did were:
- https://www.udemy.com/curso-reactjs-ninja/

## How to Run

1. Run git clone
  - ssh: git clone `git@github.com:lucasmls/mardown-editor.git`
  - https: git clone `https://github.com/lucasmls/mardown-editor.git
2. Install the dependencies with `npm install` or `yarn`.
3. Run the application with `npm start` or `yarn start`.
