import React from 'react'
import PropTypes from 'prop-types'

import EditorHeader from './EditorHeader/EditorHeader'
import Files from './Files/Files'

import { Editor, EditorContent, TextsWrapper } from './styles'

const MarkdownEditor = ({ editorValue, handleTyping, getMarkup, textareaRef, ...props }) => (
  <Editor className="editor">
    <EditorHeader handleTyping={handleTyping} { ...props } />
    <EditorContent className="editor__content">
      <Files { ...props} />
      <TextsWrapper className="texts-">
        <textarea
          ref={textareaRef}
          autoFocus
          value={editorValue}
          onChange={handleTyping}
          name="editor" cols="30" rows="10"
          data-state="value" />
        <article className="editor-view" dangerouslySetInnerHTML={getMarkup()} />
      </TextsWrapper>
    </EditorContent>
  </Editor>
)

MarkdownEditor.propTypes = {
  editorValue: PropTypes.string.isRequired,
  handleTyping: PropTypes.func.isRequired,
  getMarkup: PropTypes.func.isRequired,
}

export default MarkdownEditor