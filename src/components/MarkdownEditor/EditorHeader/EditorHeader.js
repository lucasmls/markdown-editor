import React from 'react'
import PropTypes from 'prop-types'
import FileTitle from '../Files/FileTitle/FileTitle'
import SavingFeedback from '../SavingFeedback/SavingFeedback'
import ActionButton from '../ActionButton/ActionButton'

import { Header, ActionsBox } from './styles'

const EditorHeader = ({ isSaving, removeContent, createMarkdown, selectedFileTitle, handleTyping }) => (
  <Header>
    <FileTitle handleTyping={handleTyping} title={selectedFileTitle} />
    <SavingFeedback isSaving={isSaving} />

    <ActionsBox className="actions-box">
      <ActionButton type="danger" handleClick={removeContent}>
        <img src={require('../../../assets/icons/close.svg')} alt=""/>
      </ActionButton>
      <ActionButton type="success" handleClick={createMarkdown}>
        <img src={require('../../../assets/icons/create-file.svg')} alt=""/>
      </ActionButton>
    </ActionsBox>
  </Header>
)


EditorHeader.propTypes = {
  isSaving: PropTypes.bool,
  removeContent: PropTypes.func.isRequired,
  createMarkdown: PropTypes.func.isRequired,
  selectedFileTitle: PropTypes.string.isRequired
}

export default EditorHeader;