import styled from 'styled-components'

export const Header = styled.header`
  padding: 10px;
  border-bottom: 1px solid #BDBDBD;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #F2F3F4;
`

export const ActionsBox = styled.div`
  /* margin-left: auto; */
`