import React from 'react'
import SavingFeedback from './SavingFeedback'

import { storiesOf } from '@storybook/react'

const stories = storiesOf('SavingFeedback', module)

stories.add('Saving feedback with isSaving === null', () => (
  <div>
    <p>Não renderizando nada por que o isSaving está sendo passado como nulo</p>
    <SavingFeedback isSaving={null} />
  </div>
))

stories.add('Saving feedback with isSaving === true', () => (
  <div>
    <SavingFeedback isSaving={true} />
  </div>
))

stories.add('Saving feedback with isSaving === false', () => (
  <div>
    <SavingFeedback isSaving={false} />
  </div>
))