import React from 'react'
import PropTypes from 'prop-types'

const SavingFeedback = ({ isSaving }) => (
  isSaving !== null &&
    (<span>{ isSaving ? 'Salvando...' : 'Salvo' }</span>)
)

SavingFeedback.propTypes = {
  isSaving: PropTypes.bool
}

export default SavingFeedback;