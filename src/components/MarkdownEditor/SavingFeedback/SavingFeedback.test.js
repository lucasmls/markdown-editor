import React from 'react'
import renderer from 'react-test-renderer'

import SavingFeedback from './SavingFeedback'

it('Should return null, cause no status were passed', () => {
  const tree = renderer.create(
    <SavingFeedback isSaving={null} />
    ).toJSON()
  expect(tree).toMatchSnapshot()
})

it('Should display that the file is being saved', () => {
  const tree = renderer.create(
    <SavingFeedback isSaving={true} />

  ).toJSON()
  expect(tree).toMatchSnapshot()
})

it('Should display that the file is saved', () => {
  const tree = renderer.create(
    <SavingFeedback isSaving={false} />
  ).toJSON()
  expect(tree).toMatchSnapshot()
})