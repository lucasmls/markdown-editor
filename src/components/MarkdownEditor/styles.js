import styled from 'styled-components'

export const Editor = styled.div`
  font-family: 'Lato', sans-serif;
  width: 1100px;
  height: 70%;
  border-radius: 4px;
  background-color: #fff;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
  display: flex;
  flex-direction: column;

  :hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
  }
`

export const EditorContent = styled.div`
  padding-top: 10px;
  display: flex;
  height: 100%;
`

export const TextsWrapper = styled.div`
  display: flex;
  width: 100%;

  textarea {
    flex: 1;
    outline: none;
    word-wrap: break-word;
    min-width: 50%;
    max-width: 50%;
    border: none;
    /* margin-left: 10px; */
    margin-right: 10px;
    padding-left: 10px;
    padding-right: 10px;
    border-left: 1px solid #BDBDBD;
    border-right: 1px solid #BDBDBD;
  }

  article {
    flex: 1;
    word-wrap: break-word;
    overflow: scroll;
    max-width: 50%;
    min-width: 50%;
  }
`