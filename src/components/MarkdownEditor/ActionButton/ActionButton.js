import React from 'react'
import PropTypes from 'prop-types'
import css from 'classnames'

import { Button } from './styles'

const ActionButton = ({ handleClick, children, type }) => {
  return (
    <Button className={ css({[`--${type}`]: type}) } onClick={handleClick}>
      <div>
        { children }
      </div>
    </Button>
  );
}

ActionButton.propTypes = {
  handleClick: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired
}

export default ActionButton;