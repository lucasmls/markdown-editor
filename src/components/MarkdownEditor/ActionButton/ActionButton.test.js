import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import ActionButton from './ActionButton'

configure({ adapter: new Adapter() })
it('Button with prop type = "success" should have the class "--success"', () => {
  const wrapper = shallow(
    <ActionButton type="success" onClick={() => null} />
  )

  expect(wrapper.hasClass('--success')).toBe(true)
  expect(wrapper.hasClass('--danger')).toBe(false)
})