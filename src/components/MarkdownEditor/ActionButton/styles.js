import styled from 'styled-components'

export const Button = styled.button`
  border: none;
  cursor: pointer;
  background-color: #F2F3F4;
  div {
    background-color: #BBDEFB;
    transition: .5s;
    &:hover {
      background-color: #448AFF;
      box-shadow: 0 4px 8px 0 rgba(0,0,0,0.4);
    }
    display: flex;
    justify-content: center;
    align-items: center;
    width: 35px;
    height: 35px;
    border-radius: 50%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  }

  img {
    max-width: 25px;
    max-height: 25px;
  }
`