import styled from 'styled-components'

export const FilesList = styled.div`
  height: 100%;
  min-width: 25%;
  overflow: scroll;

  ul {
    list-style: none;
    margin: 0;
    padding: 0;

    li {
      cursor: pointer;
    }
  }
`

export const FilesItem = styled.li`
  &:nth-child(even) {
    background-color: #2196F3;
  }

  &:nth-child(odd) {
    background-color: #BBDEFB;
  }

  button {
    width: 100%;
    text-align: start;
    margin: 0;
    outline: none;
    cursor: pointer;
    border: none;
    background-color: transparent;
    color: #000;
  }
`