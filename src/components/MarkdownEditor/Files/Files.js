import React from 'react'
import PropTypes from 'prop-types'

import { FilesList, FilesItem } from './styles'

const Files = ({ files, openFile }) => (
  <FilesList>
    <ul>
      {Object.keys(files).map(fileKey => (
        <FilesItem key={fileKey} onClick={openFile(fileKey)}>
          <button>
            { files[fileKey].title }
          </button>
        </FilesItem>
      ))}
    </ul>
  </FilesList>
)

Files.propTypes = {
  openFile: PropTypes.func.isRequired,
  files: PropTypes.object.isRequired
}

export default Files;