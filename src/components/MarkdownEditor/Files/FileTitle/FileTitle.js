import React from 'react'
import PropTypes from 'prop-types'

import { TitleInput } from './styles'

const FileTitle = ({ title, handleTyping }) => (
  <TitleInput value={title} data-state="selectedFileTitle" onChange={handleTyping} type="text" placeholder="Document title" />
)

FileTitle.propTypes = {
  title: PropTypes.string.isRequired
}

export default FileTitle
