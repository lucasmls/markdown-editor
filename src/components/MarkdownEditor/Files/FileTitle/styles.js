import styled from 'styled-components'

export const TitleInput = styled.input`
  outline: none;
  height: 25px;
  padding: 5px;
  border: none; 
  background: rgba(255, 255, 255, 0) no-repeat;
  background-image: linear-gradient(to bottom, #1976D2, #BBDEFB), linear-gradient(to bottom, silver, silver);
  background-size: 0 2px, 100% 1px;
  background-position: 50% 100%, 50% 100%;
  transition: background-size 0.3s cubic-bezier(0.64, 0.09, 0.08, 1);

  &:focus {
    background-size: 100% 2px, 100% 1px;
  }
`