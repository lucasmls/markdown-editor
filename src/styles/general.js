import styled from 'styled-components'

export const Body = styled.div`
  height: 100vh;
  background-color: #1976D2;
  display: flex;
  justify-content: center;
  align-items: center;
`
