import React, { Component } from 'react'
import MarkdownEditor from '../components/MarkdownEditor/MarkdownEditor'

import marked from 'marked'
import { v4 } from 'node-uuid'
import 'normalize.css'
import 'highlight.js/styles/dracula.css'

import { Body } from '../styles/general'

import('highlight.js').then(highlight => {
  marked.setOptions({
    highlight: (code, lang) => {
      if (lang && highlight.getLanguage(lang))
        return highlight.highlight(lang, code).value
      return highlight.highlightAuto(code).value
    }
  })
})

class App extends Component {
  constructor (props) {
    super(props)
    this.textareaRef = React.createRef()
    this.state = {
      value: '',
      isSaving: null,
      selectedFile: v4(),
      selectedFileTitle: 'Sem título',
      files: { }
    }
  }

  handleTyping = (e) => {
    this.setState({ [e.target.dataset.state]: e.target.value, isSaving: true })
  }

  getMarkup = () => (
    { __html: marked(this.state.value) }
  )

  createMarkdown = async () => {
    await this.setState({
      value: '',
      selectedFileTitle: 'Sem título',
      selectedFile: v4(),
      files: {
        ...this.state.files,
        [this.state.selectedFile]: { content: this.state.value, title: 'Sem título' }
      },
    })
    this.textareaRef.current.focus()
  }

  saveContent = () => {
    if (this.state.isSaving) {
      const files = {
        ...this.state.files,
        [this.state.selectedFile]: { title: this.state.selectedFileTitle, content: this.state.value }
      }
      localStorage.setItem('markdown-editor', JSON.stringify(files))
      this.setState({ isSaving: false, files })
    }
  }

  removeContent = () => {
    const { [this.state.selectedFile]: fileToRemove, ...files } = this.state.files
    localStorage.setItem('markdown-editor', JSON.stringify(files))
    this.setState({ value: '', files, selectedFileTitle: 'Sem título', selectedFile: v4() })
    this.textareaRef.current.focus()
  }

  openFile = fileKey => () => {
    this.setState({
      selectedFile: fileKey,
      value: this.state.files[fileKey].content,
      selectedFileTitle: this.state.files[fileKey].title
    })
    this.textareaRef.current.focus()
  }

  componentDidUpdate(prevProps, prevState) {
    clearInterval(this.timer)
    this.timer = setTimeout(this.saveContent, 1000)
  }

  componentWillUnmount() {
    clearInterval(this.timer)
  }

  componentDidMount() {
    const files = JSON.parse(localStorage.getItem('markdown-editor'))
    this.setState({ files: files ? files : {} })
  }

  render() {
    return (
      <Body>
        <MarkdownEditor
          editorValue={this.state.value}
          isSaving={this.state.isSaving}
          textareaRef={this.textareaRef}
          createMarkdown={this.createMarkdown}
          handleTyping={this.handleTyping}
          getMarkup={this.getMarkup}
          openFile={this.openFile}
          files={this.state.files}
          removeContent={this.removeContent}
          selectedFileTitle={this.state.selectedFileTitle} />
      </Body>
    )
  }
}

export default App